- `git clone https://gitlab.com/alblaz/EGIJuliaSeminars`
- navigate to the `EGIJuliaSeminars` folder
- start a Julia session
- type `using IJulia`
- notebook(dir = pwd(), detached = true)


